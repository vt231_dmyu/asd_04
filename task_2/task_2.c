#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#define MAX_SIZE 100

// ���� ��� ��������� �� ��������
typedef struct {
	double items[MAX_SIZE];
	int top;
} Stack;

// ������������ �����
void initialize(Stack* s) {
	s->top = -1;
}

// ��������, �� � ���� ��������
int isEmpty(Stack* s) {
	return s->top == -1;
}

// ��������, �� � ���� ������
int isFull(Stack* s) {
	return s->top == MAX_SIZE - 1;
}

// ��������� �������� �� �����
void push(Stack* s, double value) {
	if (isFull(s)) {
		printf("���� ������������!\n");
		exit(EXIT_FAILURE);
	}
	s->items[++(s->top)] = value;
}

// ��������� �������� � �����
double pop(Stack* s) {
	if (isEmpty(s)) {
		printf("���� ��������!\n");
		exit(EXIT_FAILURE);
	}
	return s->items[(s->top)--];
}

// ���������� ������������� ������ � ���������� ���������� �����
double evaluateExpression(char* expression) {
	Stack operandStack;
	initialize(&operandStack);

	char* token = strtok(expression, " ");
	while (token != NULL) {
		if (isdigit(token[0]) || (token[0] == '-' && isdigit(token[1]))) {
			// ���� �� �������, ������ ���� �� �����
			push(&operandStack, atof(token));
		}
		else {
			// ���� �� ��������, �������� �������� ��
			double operand2 = pop(&operandStack), result;
			switch (token[0]) {
			case '+':
				result = pop(&operandStack) + operand2;
				break;
			case '-':
				result = pop(&operandStack) - operand2;
				break;
			case '*':
				result = pop(&operandStack) * operand2;
				break;
			case '/':
				result = pop(&operandStack) / operand2;
				break;
			case '^':
				result = pow(pop(&operandStack), operand2);
				break;
			case 's':
				if (strcmp(token, "sqrt") == 0) {
					result = sqrt(operand2);
				}
				else {
					printf("������������� ��������: %s\n", token);
					exit(EXIT_FAILURE);
				}
				break;
			default:
				printf("������������� ��������: %s\n", token);
				exit(EXIT_FAILURE);
			}
			push(&operandStack, result);
		}
		token = strtok(NULL, " ");
	}
	return pop(&operandStack);
}

int main() {
	system("chcp 1251"); // ������� � ������ �� ��������
	system("cls"); // ������� ���� ������

	char expression[MAX_SIZE];
	printf("������ ����� � ���������� ���������� �����: ");
	fgets(expression, sizeof(expression), stdin);

	// ����� ������� ������ ����� � ���� �����
	if (expression[strlen(expression) - 1] == '\n')
		expression[strlen(expression) - 1] = '\0';

	double result = evaluateExpression(expression);
	printf("���������: %.2f\n", result);

	return 0;
}