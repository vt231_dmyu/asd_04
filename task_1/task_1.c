#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include "clist.h"

void printNode(elemtype* value) {
	printf("%d\n", *((int*)value));
}

int main() {
	system("chcp 1251"); // ������� � ������ �� ��������
	system("cls"); // ������� ���� ������

	int menu, index, exit = 1;
	cList* mylist = NULL;
	elemtype data;

	do {
		printf("======����======\n");
		printf("1. ��������� ������.\n");
		printf("2. ��������� �������� �� ������� ������.\n");
		printf("3. ��������� �������� �� ������� ������.\n");
		printf("4. ��������� �������� � ���� ������.\n");
		printf("5. ��������� �������� � ���� ������.\n");
		printf("6. ��������� �������� � �������� ���� ������.\n");
		printf("7. ��������� �������� � �������� ���� ������.\n");
		printf("8. ��������� ������� �������� ������ �� �������.\n");
		printf("9. ��������� ������ ������ �� �������.\n");
		printf("10. �������� ������.\n");
		printf("11. ��������� ������ ��������.\n");
		printf("================\n");
		printf("������ ���� � ��������� �����: "); scanf("%d", &menu);

		switch (menu) {
		case 1:
			if (mylist) {
				printf("������ ��� ���������!\n\n");
			}
			else {
				mylist = createList();
				printf("C����� ��� ������ ���������!\n\n");
			}
			break;
		case 2:
			if (mylist == NULL) {
				printf("������ �� �� ��� ���������!\n\n");
			}
			else {
				printf("������ �������� ��� ��������: "); scanf("%d", &data);
				pushFront(mylist, &data);
				printf("������� ��� ������� �� ������� ������!\n\n");
			}
			break;
		case 3:
			if (mylist == NULL) {
				printf("������ �� �� ��� ���������!\n\n");
			}
			else if (!isEmptyList(mylist)) {
				popFront(mylist, &data);
				printf("������� ��� ��������� � ������� ������!\n\n");
			}
			else {
				printf("������ ��������.\n\n");
			}
			break;
		case 4:
			if (mylist == NULL) {
				printf("������ �� �� ��� ���������!\n\n");
			}
			else {
				printf("������ �������� ��� ��������: "); scanf("%d", &data);
				pushBack(mylist, &data);
				printf("������� ��� ������� � ����� ������!\n\n");
			}
			break;
		case 5:
			if (mylist == NULL) {
				printf("������ �� �� ��� ���������!\n\n");
			}
			else if (!isEmptyList(mylist)) {
				popBack(mylist, &data);
				printf("������� ��� ��������� � ���� ������!\n\n");
			}
			else {
				printf("������ ��������.\n\n");
			}
			break;
		case 6:
			if (mylist == NULL) {
				printf("������ �� �� ��� ���������!\n\n");
			}
			else {
				printf("������ ������ ��������: "); scanf("%d", &index);
				printf("������ �������� ��� ��������: "); scanf("%d", &data);
				insertNode(mylist, index, &data);
				printf("������� ��� ������� � �������� ���� ������!\n\n");
			}
			break;
		case 7:
			if (mylist == NULL) {
				printf("������ �� �� ��� ���������!\n\n");
			}
			else if (!isEmptyList(mylist)) {
				printf("������ ������ ��������: "); scanf("%d", &index);
				removeNode(mylist, index, &data);
				printf("������� ��� ��������� � �������� ���� ������!\n\n");
			}
			else {
				printf("������ ��������.\n\n");
			}
			break;
		case 8:
			if (mylist == NULL) {
				printf("������ �� �� ��� ���������!\n\n");
			}
			else if (!isEmptyList(mylist)) {
				cNode* node;
				printf("\n======ϲ�����======\n");
				printf("1. ��������� ������� ��������.\n");
				printf("2. ��������� ���������� ��������.\n");
				printf("3. ��������� �������� �� �������.\n");
				printf("================\n");
				printf("������ ���� � ��������� �����: "); scanf("%d", &menu);
				switch (menu) {
				case 1:
					printf("������ ������� ������: %d\n\n", *(mylist->head->value));
					break;
				case 2:
					printf("�������� ������� ������: %d\n\n", *(mylist->tail->value));
					break;
				case 3:
					printf("������ ������ ��������: "); scanf("%d", &index);
					node = getNode(mylist, index);
					if (node) {
						printf("������� �� �������� %d: %d\n\n", index, *(node->value));
					}
					else {
						printf("�������� �� �������� %d - �� ����.\n\n", index);
					}
					break;
				default:
					printf("���� ����� ���� � ���������� ����.\n\n");
					break;
				}
			}
			else {
				printf("������ ��������.\n\n");
			}
			break;
		case 9:
			if (mylist == NULL) {
				printf("������ �� �� ��� ���������!\n\n");
			}
			else if (!isEmptyList(mylist)) {
				printf("������:\n");
				printList(mylist, printNode);
				printf("\n");
			}
			else {
				printf("������ ��������.\n\n");
			}
			break;
		case 10:
			if (mylist) {
				deleteList(&mylist);
				printf("C����� ��� ������ ��������!\n\n");
			}
			else {
				printf("������ �� �� ��� ���������!\n\n");
			}
			break;
		case 11:
			if (mylist) {
				printf("����� ��� �� �������� ������ �������� ������ �� ���� ��������!\n\n");
			}
			else {
				exit = 11;
			}
			break;
		default:
			printf("���� ����� ���� � ���������� ����. �������� ������.\n\n");
			break;
		}
	} while (menu != 11 || exit != 11);

	return 0;
}
