#include "clist.h"

// ��������� ���������� ������
cList* createList(void) {
    cList* list = (cList*)malloc(sizeof(cList));
    if (list) {
        list->size = 0;
        list->head = list->tail = NULL;
    }
    return list;
}

// ��������� ������
void deleteList(cList** list) {
    cNode* head = (*list)->head;
    cNode* next = NULL;
    while (head) {
        next = head->next;
        free(head);
        head = next;
    }
    free(*list);
    *list = NULL;
}

// �������� ������ �� ���������
bool isEmptyList(cList* list) {
    return ((list->head == NULL) || (list->tail == NULL));
}

// ��������� ������ ����� �� ������� ������
int pushFront(cList* list, elemtype* data) {
    cNode* node = (cNode*)malloc(sizeof(cNode));
    if (!node) {
        return(-1);
    }
    node->value = (elemtype*)malloc(sizeof(elemtype)); // �������� ���'�� ��� ��������
    if (!node->value) {
        free(node);
        return(-1);
    }
    *(node->value) = *data; // ��������� ��������
    node->next = list->head;
    node->prev = NULL;

    if (!isEmptyList(list)) {
        list->head->prev = node;
    }
    else {
        list->tail = node;
    }
    list->head = node;

    list->size++;
    return(0);
}

// ��������� ����� � ������� ������
int popFront(cList* list, elemtype* data) {
    cNode* node;

    if (isEmptyList(list)) {
        return(-2);
    }

    node = list->head;
    list->head = list->head->next;

    if (!isEmptyList(list)) {
        list->head->prev = NULL;
    }
    else {
        list->tail = NULL;
    }

    data = node->value;
    list->size--;
    free(node);

    return(0);
}

// ��������� ������ ����� � ����� ������
int pushBack(cList* list, elemtype* data) {
    cNode* node = (cNode*)malloc(sizeof(cNode));
    if (!node) {
        return(-3);
    }
    node->value = (elemtype*)malloc(sizeof(elemtype)); // �������� ���'�� ��� ��������
    if (!node->value) {
        free(node);
        return(-3);
    }
    *(node->value) = *data; // ��������� ��������
    node->next = NULL;
    node->prev = list->tail;
    if (!isEmptyList(list)) {
        list->tail->next = node;
    }
    else {
        list->head = node;
    }
    list->tail = node;

    list->size++;
    return(0);
}

// ����� ����� � ���� ������
int popBack(cList* list, elemtype* data) {
    cNode* node = NULL;

    if (isEmptyList(list)) {
        return(-4);
    }

    node = list->tail;
    list->tail = list->tail->prev;
    if (!isEmptyList(list)) {
        list->tail->next = NULL;
    }
    else {
        list->head = NULL;
    }

    data = node->value;
    list->size--;
    free(node);

    return(0);
}

// ��������� ������ ����� � ������ ���� ������
int insertNode(cList* list, int index, elemtype* data) {
    if (index < 0 || index > list->size) {
        return (-5);
    }

    if (index == 0) {
        return pushFront(list, data);
    }
    else if (index == list->size) {
        return pushBack(list, data);
    }
    else {
        cNode* newNode = (cNode*)malloc(sizeof(cNode));
        if (!newNode) {
            return (-1);
        }
        newNode->value = (elemtype*)malloc(sizeof(elemtype)); // �������� ���'�� ��� ��������
        if (!newNode->value) {
            free(newNode);
            return(-1);
        }
        *(newNode->value) = *data; // ��������� ��������

        cNode* node = getNode(list, index);
        newNode->next = node;
        newNode->prev = node->prev;
        node->prev->next = newNode;
        node->prev = newNode;

        list->size++;
        return 0;
    }
}

// ��������� ����� � �������� ���� ������
int removeNode(cList* list, int index, elemtype* data) {
    if (index < 0 || index >= list->size) {
        return (-6);
    }

    if (index == 0) {
        return popFront(list, data);
    }
    else if (index == list->size - 1) {
        return popBack(list, data);
    }
    else {
        cNode* node = getNode(list, index);
        node->prev->next = node->next;
        node->next->prev = node->prev;

        data = node->value;
        free(node);

        list->size--;
        return 0;
    }
}

// ������� ��������� ����� ������
cNode* getNode(cList* list, int index) {
    cNode* node = NULL;
    int i;

    if (index >= list->size) {
        return (NULL);
    }

    if (index < list->size / 2) {
        i = 0;
        node = list->head;
        while (node && i < index) {
            node = node->next;
            i++;
        }
    }
    else {
        i = list->size - 1;
        node = list->tail;
        while (node && i > index) {
            node = node->prev;
            i--;
        }
    }
    return node;
}

// ��������� ������ � �������
void printList(cList* list, void (*func)(elemtype*)) {
    cNode* node = list->head;

    if (isEmptyList(list)) {
        return;
    }

    while (node) {
        func(node->value);
        node = node->next;
    }
}
